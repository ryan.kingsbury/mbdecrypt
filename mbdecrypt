#!/usr/bin/env python3

import argparse
import email
from email import policy
import gpg
import mailbox

def decrypt(message):
    if message.get_content_type() != "multipart/encrypted":
        raise ValueError("Can't decrypt an unencrypted message")
    cipher = message.get_payload()[1].get_payload()
    with gpg.Context() as c:
        try:
            decrypted = c.decrypt(cipher.encode("utf-8"))[0]
        except gpg.errors.GPGMEError:
            decrypted = None
    return decrypted

class MBDecrypt:
    def __init__(self, path_encrypted, path_decrypted, mbtype):
        self.mbtype = mbtype
        self.num_errors = 0
        if mbtype == "mbox":
            self.inbox = mailbox.mbox(path_encrypted, create=False)
            self.outbox = mailbox.mbox(path_decrypted)
        elif mbtype == "maildir":
            self.inbox = mailbox.Maildir(path_encrypted, create=False)
            self.outbox = mailbox.Maildir(path_decrypted)
        else:
            print("Error: Mailbox type needs to be \"mbox\" or \"maildir\"")
            exit(1)
        self.existing_msg_ids = []
        for i, msg in enumerate(self.outbox.values()):
            self.existing_msg_ids.append(msg.get("Message-ID"))

    def _copy_mbtype_specific(self, msg_encrypted, msg_decrypted):
        if (self.mbtype == "mbox"):
            msg_decrypted.set_from(msg_encrypted.get_from())
        else:
            msg_decrypted.set_subdir(msg_encrypted.get_subdir())

    def _get_mbtype_message(self, msg):
        email_msg = email.message_from_bytes(msg)
        if (self.mbtype == "mbox"):
            return mailbox.mboxMessage(email_msg)
        else:
            return mailbox.MaildirMessage(email_msg)

    def decrypt(self):
        total_messages = len(self.inbox.values())
        for i, msg_encrypted in enumerate(self.inbox.values()):
            print("\rDecrypting... %d/%d" % (i + 1, total_messages), end="")
            if msg_encrypted.get("Message-ID") in self.existing_msg_ids:
                continue
            if msg_encrypted.get_content_type() != "multipart/encrypted":
                self.outbox.add(msg_encrypted)
                continue
            msg_decrypted = decrypt(msg_encrypted)
            if not msg_decrypted:
                self.num_errors += 1
                continue
            msg_decrypted = self._get_mbtype_message(msg_decrypted)
            self._copy_mbtype_specific(msg_encrypted, msg_decrypted)
            msg_decrypted.add_header("From", msg_encrypted.get("From"))
            msg_decrypted.add_header("To", msg_encrypted.get("To"))
            msg_decrypted.add_header("Subject", msg_encrypted.get("Subject"))
            msg_decrypted.add_header("Date", msg_encrypted.get("Date"))
            msg_decrypted.add_header("Message-ID",
                                     msg_encrypted.get("Message-ID"))
            msg_decrypted.set_flags(msg_encrypted.get_flags())
            self.outbox.add(msg_decrypted)
        print()
        if self.num_errors > 0:
            print("[ERROR] %d messages have not been decrypted because GPG "
                  "returned an error" % self.num_errors)

def main():
    parser = argparse.ArgumentParser(description="Decrypt an entire mailbox")
    parser.add_argument("type", help="Type of mailbox (mbox or maildir)")
    parser.add_argument("path_encrypted", help="Path to the encrypted mailbox")
    parser.add_argument("path_decrypted", help="Path to the decrypted mailbox")
    args = parser.parse_args()
    mbdecrypt = MBDecrypt(args.path_encrypted, args.path_decrypted, args.type)
    mbdecrypt.decrypt()

if __name__ == "__main__":
    main()
